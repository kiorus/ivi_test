import requests
import json

from RestApiAuth import RestApiAuth


class RestApiActions(RestApiAuth):

    __headers = {'Content-type': 'application/json'}

    def __create_json_data(self, name, universe, education, weight, height, identity, other_aliases):
        data = {'name': name,
                'universe': universe,
                'education': education,
                'weight': weight,
                'height': height,
                'identity': identity,
                'other_aliases': other_aliases}
        json_data = json.dumps(data)
        return json_data

    def create_character(self, name, universe, education, weight, height, identity, other_aliases):
        json_data = self.__create_json_data(name, universe, education, weight, height, identity, other_aliases)

        response = requests.post(super().get_host() + "/v2/character", data=json_data, auth=super().get_auth(), headers=self.__headers)
        return response, json_data

    def update_character(self, name, universe, education, weight, height, identity, other_aliases):
        json_data = self.__create_json_data(name, universe, education, weight, height, identity, other_aliases)

        response = requests.put(super().get_host() + "/v2/character", data=json_data, auth=super().get_auth(), headers=self.__headers)
        return response, json_data

    def delete_character(self, name):
        response = requests.delete(super().get_host() + "/v2/character?name=" + name, auth=super().get_auth())
        return response

    def reset_characters(self):
        response = requests.post(super().get_host() + "/v2/reset", auth=super().get_auth())
        return response
