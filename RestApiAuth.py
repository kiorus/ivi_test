import os


class RestApiAuth:

    __hostName = "http://rest.test.ivi.ru"
    __login = ""
    __password = ""

    def __init__(self):
        self.__login = os.environ['IVI_LOGIN']
        self.__password = os.environ['IVI_PASSWORD']

    def get_host(self):
        return self.__hostName

    def get_auth(self):
        return tuple((self.__login, self.__password))
