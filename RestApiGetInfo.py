import requests

from RestApiAuth import RestApiAuth


class RestApiGetInfo(RestApiAuth):

    def get_all_characters_info(self):
        response = requests.get(super().get_host() + "/v2/characters", auth=super().get_auth())
        return response

    def get_character_by_name(self, name):
        response = requests.get(super().get_host() + "/v2/character?name=" + name, auth=super().get_auth())
        return response
