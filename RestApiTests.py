import json
import random
import string

from RestApiActions import RestApiActions
from RestApiGetInfo import RestApiGetInfo


def random_string(string_length):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(string_length))


api_get_info = RestApiGetInfo()
api_actions = RestApiActions()


def test_reset_characters():
    response = api_actions.reset_characters()
    assert response.status_code == 200  # проверка, что запрос на восстановление завершился успешно

    response = api_get_info.get_all_characters_info()
    assert len(response.json()['result']) == 302  # проверка количества персонажей


character_name = random_string(8)  # генерация имени персонажа для последующего использования


def test_create_character():
    response, json_data = api_actions.create_character(character_name,
                                                       "Какая ещё вселенная? Местный я ...",
                                                       "3 класса начальной школы",
                                                       59,
                                                       164.12,
                                                       "Абракадабра",
                                                       "Человек-Головоломка")
    assert response.json()['result']['name']            == json.loads(json_data)['name']
    assert response.json()['result']['universe']        == json.loads(json_data)['universe']
    assert response.json()['result']['education']       == json.loads(json_data)['education']
    assert response.json()['result']['weight']          == json.loads(json_data)['weight']
    assert response.json()['result']['height']          == json.loads(json_data)['height']
    assert response.json()['result']['identity']        == json.loads(json_data)['identity']
    assert response.json()['result']['other_aliases']   == json.loads(json_data)['other_aliases']


def test_create_same_character():
    __json = "{\"error\":\"" + character_name + " is already exists\"}\n"
    response, json_data = api_actions.create_character(character_name,
                                                       "qwe",
                                                       "asd",
                                                       12,
                                                       123123,
                                                       "zxc",
                                                       "qaz")
    assert response.text == __json


def test_update_character():
    response, json_data = api_actions.update_character(character_name,
                                                       "Вселенная вечных апдейтов",
                                                       "старшая школа",
                                                       599.1111,
                                                       1644.1212,
                                                       "ololo",
                                                       "Человек-Жиринка")
    assert response.json()['result']['name']            == json.loads(json_data)['name']
    assert response.json()['result']['universe']        == json.loads(json_data)['universe']
    assert response.json()['result']['education']       == json.loads(json_data)['education']
    assert response.json()['result']['weight']          == json.loads(json_data)['weight']
    assert response.json()['result']['height']          == json.loads(json_data)['height']
    assert response.json()['result']['identity']        == json.loads(json_data)['identity']
    assert response.json()['result']['other_aliases']   == json.loads(json_data)['other_aliases']


def test_delete_character():
    __json = "{\"result\":\"Hero " + character_name + " is deleted\"}\n"
    response = api_actions.delete_character(character_name)  # удаление существующего персонажа
    assert response.text == __json


def test_delete_blank_character():
    __json = "{\"error\":\"No such name\"}\n"
    response = api_actions.delete_character(character_name)  # повторное удаление уже не существующего персонажа
    assert response.text == __json


def test_delete_all_characters():
    response = api_get_info.get_all_characters_info()
    counter = len(response.json()['result'])

    for count in range(0, counter):
        name = response.json()['result'][count]['name']
        api_actions.delete_character(name)

    response = api_get_info.get_all_characters_info()
    assert len(response.json()['result']) == 0  # проверка, что сервис не умер после удаления последнего персонажа


def test_create_more_500_characters():
    __json = "{\"error\":\"Collection can't contain more than 500 items\"}\n"

    api_actions.reset_characters()
    response = api_get_info.get_all_characters_info()
    counter = len(response.json()['result'])

    for count in range(0, 500-counter):
        api_actions.create_character(character_name + str(count),
                                     character_name + str(count),
                                     character_name + str(count),
                                     50,
                                     100.7,
                                     character_name + str(count),
                                     character_name + str(count))
    response = api_get_info.get_all_characters_info()
    counter = len(response.json()['result'])
    assert counter == 500  # проверка, что мы находимся на верхней границе коллекции

    response, json_data = api_actions.create_character(character_name + str(501),
                                                       character_name + str(501),
                                                       character_name + str(501),
                                                       50,
                                                       100.7,
                                                       character_name + str(501),
                                                       character_name + str(501))
    assert response.text == __json  # проверка, что появилось сообщение о попытке превысить верхнюю границу коллекции

